package kubeiaas.iaascore.scheduler;

import com.alibaba.fastjson2.JSON;
import kubeiaas.common.bean.Device;
import kubeiaas.common.bean.Host;
import kubeiaas.common.bean.Vm;
import kubeiaas.common.constants.ResponseMsgConstants;
import kubeiaas.common.enums.device.DeviceStatusEnum;
import kubeiaas.common.enums.vm.VmStatusEnum;
import kubeiaas.iaascore.config.AgentConfig;
import kubeiaas.iaascore.dao.TableStorage;
import kubeiaas.iaascore.dao.feign.DeviceController;
import kubeiaas.iaascore.exception.BaseException;
import kubeiaas.iaascore.response.ResponseEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

@Slf4j
@Configuration
public class DeviceScheduler {
    @Resource
    private DeviceController deviceController;

    @Resource
    private TableStorage tableStorage;

    public List<Device> queryAll(Host host) throws BaseException {
        // 1. Devices from host RAW
        List<Device> rawDevices;
        try {
            URI uri = getUri(host);
            log.info(String.format("URI: %s", uri));
            String jsonObjectString = deviceController.queryAll(uri);
            rawDevices = JSON.parseArray(jsonObjectString, Device.class);
        } catch (Exception e) {
            log.error(String.format("ERROR: device query all failed, host name is %s.", host.getName()), e);
            throw new BaseException("err: device query all failed, host name is " + host.getName());
        }

        // 2. Devices from DB
        List<Device> dbDevices = tableStorage.deviceQueryAllByHostUuid(host.getUuid());

        // 3. build total (device amount will not too large, this method O(n^2) is ok)
        List<Device> deviceList = new ArrayList<>();
        for (Device rawDev : rawDevices) {
            boolean addFlag = false;
            for (Device dbDev : dbDevices) {
                if (rawDev.equals(dbDev)) {
                    deviceList.add(dbDev);
                    dbDevices.remove(dbDev);
                    addFlag = true;
                    break;
                }
            }
            if (!addFlag) deviceList.add(rawDev);
        }

        // 3.1. set UNREACHABLE
        for (Device dbDev : dbDevices) {
            dbDev.setStatus(DeviceStatusEnum.UNREACHABLE);
        }
        deviceList.addAll(dbDevices);

        // 3.2. set SIGN
        for (Device device : deviceList) {
            device.setSign(device.encodeSign());
        }

        return deviceList;
    }

    public List<Device> queryByVm(Vm vm) throws BaseException {
        // 1. Devices from DB
        log.info(String.format("-> invoke DB -- deviceQueryByVmUuid, vm uuid: %s", vm.getUuid()));
        List<Device> dbDevices = tableStorage.deviceQueryByVmUuid(vm.getUuid());
        log.info("<- invoke DB -- done");

        log.info(String.format("dbDevices: %s", dbDevices));
        if (CollectionUtils.isEmpty(dbDevices)) {
            return new ArrayList<>();
        }

        // 2. Devices form host RAW
        log.info(String.format("-> invoke DB -- hostQueryByUuid, host uuid: %s", vm.getHostUuid()));
        Host host = tableStorage.hostQueryByUuid(vm.getHostUuid());
        log.info("<- invoke DB -- done");

        List<Device> rawDevices;
        try {
            log.info(String.format("-> invoke AGENT -- queryAll, host name: %s", host.getName()));
            String jsonObjectString = deviceController.queryAll(getUri(host));
            rawDevices = JSON.parseArray(jsonObjectString, Device.class);
            log.info("<- invoke AGENT -- done");
        } catch (Exception e) {
            log.error(String.format("ERROR: device query all failed, host name is %s. Exception: %s", host.getName(), e));
            throw new BaseException("err: device query all failed, host name is " + host.getName());
        }

        // 3. build total (device amount will not too large, this method O(n^2) is ok)
        log.info("check device status");
        List<Device> deviceList = new ArrayList<>();
        for (Device dbDev : dbDevices) {
            boolean findFlag = false;
            for (Device rawDev : rawDevices) {
                if (rawDev.equals(dbDev)) {
                    log.info("Get device: %s", dbDev);
                    deviceList.add(dbDev);
                    findFlag = true;
                    break;
                }
            }
            if (!findFlag) {
                log.info("Device not found");
                dbDev.setStatus(DeviceStatusEnum.UNREACHABLE);
                deviceList.add(dbDev);
            }
        }
        log.info("Device status checked!");
        // 3.1. set SIGN
        for (Device device : deviceList) {
            device.setSign(device.encodeSign());
        }


        return deviceList;
    }

    public void attachDevice(Device attachDevice, Host host, Vm vm) throws BaseException {
        if (!checkOperate(vm, attachDevice)) {
            log.error("ERROR: device operate unavailable on VM (not support VM status)!");
            throw new BaseException(
                    "err: device operate unavailable on VM (not support VM status)!", ResponseEnum.DEVICE_ATTACH_ERROR);
        }
        List<Device> deviceList = this.queryAll(host);
        log.info(String.format("device to attach: %s", attachDevice));
        for (Device device : deviceList) {
            if (device.equals(attachDevice) && device.getStatus().equals(DeviceStatusEnum.AVAILABLE)) {
                // attach device
                if (!deviceController.attach(JSON.toJSONString(device), JSON.toJSONString(vm), getUri(host))
                        .equals(ResponseMsgConstants.SUCCESS)) {
                    log.error("ERROR: AGENT do attach failed!");
                    throw new BaseException(
                            "err: AGENT do attach failed!", ResponseEnum.DEVICE_ATTACH_ERROR);
                }
                device.setStatus(DeviceStatusEnum.ATTACHED);
                device.setInstanceUuid(vm.getUuid());
                log.info(String.format("-> invoke DB -- deviceSave, set status %s", device.getStatus()));
                tableStorage.deviceSave(device);
                log.info("<- invoke DB -- done");
                return;
            }
        }
        log.error("ERROR: device not found!");
        throw new BaseException(
                "err: device not found!", ResponseEnum.DEVICE_ATTACH_ERROR);
    }

    public void detachDevice(Device detachDevice, Host host, Vm vm) throws BaseException {
        if (!checkOperate(vm, detachDevice)) {
            log.error("ERROR: device operate unavailable on VM (not support VM status)!");
            throw new BaseException(
                    "err: device operate unavailable on VM (not support VM status)!", ResponseEnum.DEVICE_ATTACH_ERROR);
        }
        List<Device> deviceList = tableStorage.deviceQueryByVmUuid(vm.getUuid());
        log.info(String.format("Get device list: %s", deviceList));
        log.info(String.format("Device to detach: %s", detachDevice));
        for (Device device : deviceList) {
            // from DB only has ATTACHED, no UNREACHABLE
//            device.setType(DeviceTypeEnum.MDEV);
            if (device.equals(detachDevice) && device.getStatus().equals(DeviceStatusEnum.ATTACHED)) {
                // detach device
                if (!deviceController.detach(JSON.toJSONString(device), JSON.toJSONString(vm), getUri(host))
                        .equals(ResponseMsgConstants.SUCCESS)) {
                    log.error("ERROR: AGENT do detach failed!");
                    throw new BaseException(
                            "err: AGENT do detach failed!", ResponseEnum.DEVICE_DETACH_ERROR);
                }
                // delete in db
                if (!tableStorage.deviceDelete(device)) {
                    log.error("ERROR: DB do delete failed!");
                    throw new BaseException(
                            "err: DB do delete failed!", ResponseEnum.DEVICE_DETACH_ERROR);
                }
                return;
            }
        }
        log.error("ERROR: device not found!");
        throw new BaseException(
                "err: device not found!", ResponseEnum.DEVICE_DETACH_ERROR);
    }

    public void deleteDevice(String vmUuid) throws BaseException {
        List<Device> deviceList = tableStorage.deviceQueryByVmUuid(vmUuid);
        for (Device device : deviceList) {
            if (!tableStorage.deviceDelete(device)) {
                log.error(String.format("ERROR: device delete failed! id: %s",device.getId()));
                throw new BaseException(
                        "error: device delete failed! id: " + device.getId());
            }
        }
    }

    /**
     * check if device operate available on vm
     */
    private boolean checkOperate(Vm vm, Device device) {
        switch (device.getType()) {
            case USB:
                return true;
            case PCI:
            case MDEV:
                return vm.getStatus().equals(VmStatusEnum.STOPPED);
        }
        return false;
    }

    private URI getUri(Host host) {
        try {
            return new URI(AgentConfig.getHostUri(host));
        } catch (URISyntaxException e) {
            log.error("ERROR: build URI failed! %s", e);
            return null;
        }
    }

}
