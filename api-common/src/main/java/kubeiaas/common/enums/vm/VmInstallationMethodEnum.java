package kubeiaas.common.enums.vm;

public enum VmInstallationMethodEnum {
    SNAPSHOT, ISO;
}
