package kubeiaas.common.constants.bean;

public class ImageConstants {
    /* DB key */
    public static final String UUID = "uuid";

    /* config */
    public static final String IMAGE_PATH = "images/";

    /* statistics */
    public static final String TOTAL = "total";

    // suffix
    public static final String IMAGE_YAML_SUFFIX = ".yaml";

    // image upload status
    public static final String IMAGE_UPLOAD_STATUS = "status";
}
