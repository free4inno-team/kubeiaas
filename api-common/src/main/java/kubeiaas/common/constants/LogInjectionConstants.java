package kubeiaas.common.constants;

/**
 * Constants for Log Injection
 */
public class LogInjectionConstants {
    public static final String REQUEST_URI_INJECTION = "requestURI";
    public static final String UUID_INJECTION = "uuidInjection";
}
