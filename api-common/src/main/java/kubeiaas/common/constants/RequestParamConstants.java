package kubeiaas.common.constants;

/**
 * Constants for HTTP Request Param Constants
 */
public class RequestParamConstants {
    public static final String NAME = "name";
    public static final String UUID = "uuid";
    public static final String DESCRIPTION = "description";
    public static final String ID = "id";
    public static final String OBJECT = "object";

    public static final String IMAGE_UUID = "image_uuid";
    public static final String HOST_UUID = "host_uuid";
    public static final String HOST_NAME = "host_name";
    public static final String VM_UUID = "vm_uuid";
    public static final String VOLUME_UUID = "volume_uuid";
    public static final String INSTANCE_UUID = "instance_uuid";
    public static final String BOOT_ORDER = "boot_order";

    public static final String KEY_1 = "key_1";
    public static final String VALUE_1 = "value_1";
    public static final String KEY_2 = "key_2";
    public static final String VALUE_2 = "value_2";
    public static final String KEYWORDS = "keywords";

    public static final String STRATEGY = "strategy";
    public static final String STATUS = "status";
    public static final String ADDRESS = "address";
    public static final String TYPE = "type";

    public static final String PAGE_NUM = "page_num";
    public static final String PAGE_SIZE = "page_size";

    /* ===== VM ===== */
    public static final String XML_DESC = "xml_desc";
    public static final String VM_OBJECT = "vm_object";

    public static final String CPUS = "cpus";
    public static final String MEMORY = "memory";
    public static final String DISK_SIZE = "disk_size";

    public static final String USER = "user";
    public static final String PASSWD = "passwd";

    /* ===== IMAGE ===== */
    public static final String IMAGE_OBJECT = "image_object";
    public static final String CONTENT = "content";

    /* ===== IP ===== */
    public static final String IP_SEGMENT_ID = "ip_segment_id";
    public static final String PRIVATE_IP_SEGMENT_ID = "private_ip_segment_id";
    public static final String PUBLIC_IP_SEGMENT_ID = "public_ip_segment_id";
    public static final String IP_SEGMENT_OBJECT = "ip_segment_object";
    public static final String IP_USED_OBJECT = "ip_used_object";
    public static final String MAC = "mac";
    public static final String IP = "ip";
    public static final String HOST_URI = "host_uri";

    /* ===== HOST BRIDGE ===== */
    public static final String HOST_BRIDGE_OBJECT = "host_bridge_object";
    public static final String HOST_BRIDGE_ID = "host_bridge_id";

    /* ===== VOLUME ===== */
    public static final String VOLUME_ID = "volume_id";
    public static final String VOLUME_OBJECT = "volume_object";
    public static final String EXTRA_SIZE = "extra_size";
    public static final String VOLUME_SIZE = "volume_size";
    public static final String IMAGE_PATH = "image_path";
    public static final String VOLUME_PATH = "volume_path";
    public static final String VOLUME_TYPE = "volume_type";

    /* ===== ISO ===== */
    public static final String ISO_UUID = "iso_uuid";
    public static final String ISO_OBJECT = "iso_object";
    public static final String MOUNT_OBJECT = "mount_object";

    public static final String FILE_URL = "file_url";

    /* ===== HOST ===== */
    public static final String HOST_OBJECT = "host_object";

    /* ===== SERVICE =====*/
    public static final String SERVICE_NAME = "service_name";
    public static final String NODE_NAME = "node_name";
    public static final String TIMESTAMP = "timestamp";

    /* ==== DEVICE ===== */
    public static final String DEVICE_OBJECT = "device_object";
}
